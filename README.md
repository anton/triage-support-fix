# Support Team Contributions weekly summary

This project posts a weekly summary of newly merged ~"Support Team Contributions"
in the [Support Team Meta](https://gitlab.com/gitlab-com/support/support-team-meta)
issue tracker.

The goal is to allow easy visibility into documentation and code contributions
made by Support team members.

## Method

The [`gitlab-triage` gem](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage)
is used together with a [scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
to generate the weekly summary. The summary contents are configured in the
`.triage-policies.yml` file.

A [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html),
owned by the Support Team Meta project, is used to post the weekly summary. 

## Development

You can use the manually-triggered `dry-run:triage` job to test triage policy
changes without actually posting a summary in the target project.

## Maintainers

* @jfarmiloe
* @weimeng
